var N = 3;
function XO(el) {
	this.el = el;
	this.el.className = "xo";
	this.init();

	this.el.addEventListener('click', function(event) {
		var target = event.target;
		var l = target.getAttribute('data-line');
		var c = target.getAttribute('data-col');
		if (l == null || c == null || this.m[l][c] != 0) {
			return;
		}
		this.m[l][c] = this.turn;
		this.turn = this.turn === 1 ? 2 : 1;
		this.render();
		this.check();
	}.bind(this));
}
XO.prototype.init = function() {
	this.m = [];
	this.turn = 1;
	for (var i = 0; i < N; i++) {
		this.m[i] = Array(N);
		for (var j = 0; j < N; j++) {
			this.m[i][j] = 0;
		}
	}
	this.render();
};
XO.prototype.checkDiag = function() {
	if (this.m[0][0] && this.m[0][0] === this.m[1][1] && this.m[1][1] === this.m[2][2]) {
		return this.m[0][0];
	}
	if (this.m[0][2] && this.m[0][2] === this.m[1][1] && this.m[1][1] === this.m[2][0]) {
		return this.m[0][2];
	}
	return 0;
};
XO.prototype.checkCol = function(c) {
	if (this.m[0][c] && this.m[0][c] === this.m[1][c] && this.m[1][c] === this.m[2][c]) {
		return this.m[0][c];
	}
	return 0;
};
XO.prototype.checkLine = function(l) {
	if (this.m[l][0] && this.m[l][0] === this.m[l][1] && this.m[l][0]=== this.m[l][2]) {
		return this.m[l][0];
	}
	return 0;
};
XO.prototype.check = function() {
	var winner = this.checkLine(0) || this.checkLine(1) || this.checkLine(2) ||
		this.checkCol(0) || this.checkCol(1) || this.checkCol(2) || this.checkDiag();
	if (winner) {
		alert("Winner is: " + (winner === 1 ? "X" : "O"));
		this.init();
	} else if (!this.movesAllowed()) {
		alert("Draw");
		this.init();
	}
};
XO.prototype.movesAllowed = function() {
	for (var i = 0; i < N; i++) {
		for (var j = 0; j < N; j++) {
			if (!this.m[i][j]) return true;
		}
	}
	return false;
};
XO.prototype.render = function() {
	var MAP = ["", "X", "O"];
	var x = "";
	for (var i = 0; i < N; i++) {
		for (var j = 0; j < N; j++) {
			x += '<div data-line="' + i + '" data-col="' + j + '">' + MAP[this.m[i][j]] + '</div>';
		}
	}
	this.el.innerHTML = x;
};
